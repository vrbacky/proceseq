#!/usr/bin/env python3

import unittest
from proceseq import most_distant
from collections import Counter


class test_ExtractHamming(unittest.TestCase):

    def setUp(self):
        sequences = {"p7-01": "TAATGGAG", "p7-02": "TGATAGTG",
                     "p7-03": "ATCAAAGG", "p7-04": "CACCGGGA",
                     "p7-05": "CCGTAGTT"}
        self.test = most_distant.MostDistant(3, sequences, quiet=True)
        self.test.most_distant(False)

        sequences = {"p7-01": "TAATGGAG", "p7-02": "TGATAGTG",
                     "p7-03": "ATCAAAGG", "p7-04": "CACCGGGA",
                     "p7-05": "CCGTAGTT", "p7-06": "TGTCGAAA",
                     "p7-07": "ACTGCCAT", "p7-08": "TTGGCCAC"}
        self.test2 = most_distant.MostDistant(4, sequences, quiet=True)
        self.test2.most_distant(False)

    def test_hamdist_returns_proper_values(self):
        self.assertEqual(self.test.hamdist("AAAAAAA", "AAAAAAA"), 0)
        self.assertEqual(self.test.hamdist("AAAAAAA", "AAACAAA"), 1)
        self.assertEqual(self.test.hamdist("AAAAAAA", "AAACABA"), 2)
        self.assertRaises(
                          AssertionError,
                          self.test.hamdist, "AAAAAAA", "AAAAAA")

    def test_count_mindist_returns_proper_value(self):
        sequences = ["AAAAA", "XAABC", "ZZZZZ"]
        self.assertEqual(self.test.count_mindist(sequences), 3)
        sequences = ["AAAAA", "XAABC", "ZZZZZW"]
        self.assertRaises(AssertionError, self.test.count_mindist, sequences)

    def test_find_distant_sequences_returns_proper_values(self):
        self.assertEqual(self.test.find_distant_sequences(),
                         ([['p7-01', 'p7-03', 'p7-05'],
                           ['p7-02', 'p7-03', 'p7-04'],
                           ['p7-03', 'p7-04', 'p7-05']],
                          6))
        self.assertEqual(self.test.find_distant_sequences(None, 4),
                         ([['p7-02', 'p7-03', 'p7-05'],
                           ['p7-02', 'p7-04', 'p7-05']],
                          4))

    def test_compare_colors_returns_proper_values(self):
        self.assertEqual(self.test.compare_colors(["TGATAGTG",
                                                   "CACCGGGA",
                                                   "ACTGCCAT"]),
                         0)
        self.assertEqual(self.test.compare_colors(["TAATGGAG",
                                                   "TGATAGTG",
                                                   "ATCAAAGG"]),
                         1)
        self.assertEqual(self.test.compare_colors(["XAATGGAG",
                                                   "TGATAGTG",
                                                   "ATCAAAGG"]),
                         2)

    def test_color_heterogeneinty_returns_proper_values(self):
        self.assertEqual(self.test.color_heterogeneity(), ([], 0))

        self.assertEqual(self.test2.color_heterogeneity(), (
                [['p7-01', 'p7-03', 'p7-05', 'p7-08'],
                 ['p7-02', 'p7-03', 'p7-04', 'p7-07'],
                 ['p7-02', 'p7-03', 'p7-04', 'p7-08'],
                 ['p7-02', 'p7-03', 'p7-06', 'p7-07'],
                 ['p7-03', 'p7-04', 'p7-05', 'p7-08'],
                 ['p7-03', 'p7-05', 'p7-06', 'p7-07'],
                 ['p7-03', 'p7-05', 'p7-06', 'p7-08']], 6))

    def test_count_distances_returns_right_value(self):

        right_result = {3: {('p7-05', 'p7-02', 'p7-01'),
                            ('p7-04', 'p7-02', 'p7-01'),
                            ('p7-02', 'p7-01', 'p7-03')},
                        4: {('p7-05', 'p7-04', 'p7-02'),
                            ('p7-05', 'p7-02', 'p7-03')},
                        5: {('p7-05', 'p7-04', 'p7-01'),
                            ('p7-04', 'p7-01', 'p7-03')},
                        6: {('p7-04', 'p7-02', 'p7-03'),
                            ('p7-05', 'p7-04', 'p7-03'),
                            ('p7-05', 'p7-01', 'p7-03')}}

        result = self.test.count_distances()
        self.assertEqual(result.keys(), right_result.keys())
        for (key, seq_combinations) in result.items():
            self.assertEqual(len(seq_combinations), len(right_result[key]))
            counted_right = [Counter(i) for i in right_result[key]]
            self.assertTrue(all(Counter(seq_combination) in counted_right
                                for seq_combination in seq_combinations))

        self.test.count = 4
        right_result = {3: {('p7-05', 'p7-01', 'p7-04', 'p7-02'),
                            ('p7-05', 'p7-01', 'p7-03', 'p7-02'),
                            ('p7-01', 'p7-04', 'p7-03', 'p7-02')},
                        4: {('p7-05', 'p7-04', 'p7-03', 'p7-02')},
                        5: {('p7-05', 'p7-01', 'p7-04', 'p7-03')}}
        result = self.test.count_distances()
        self.assertEqual(result.keys(), right_result.keys())
        for (key, seq_combinations) in result.items():
            self.assertEqual(len(seq_combinations), len(right_result[key]))
            counted_right = [Counter(i) for i in right_result[key]]
            self.assertTrue(all(Counter(seq_combination) in counted_right
                                for seq_combination in seq_combinations))


if __name__ == "__main__":
    unittest.main()
