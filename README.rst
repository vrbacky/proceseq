========
ProceSeq
========

ProceSeq is toolkit for processing of fasta/fastq files.

Requirements
============

- `Python >= 3.4.0 <http://python.org>`__
- `Biopython >= 1.65 <http://biopython.org>`__

Download
========

The latest stable release of ProceSeq may be downloaded from
`Bitbucket <https://bitbucket.org/vrbacky/proceseq/downloads>`__.

Development versions and source code are available on
`Bitbucket <https://bitbucket.org/vrbacky/proceseq/overview>`__.

Installation
============

The simplest way to install the latest stable release is via pip

   $ pip3 install proceseq --user
 
Linux
-----

1. Clone ProceSeq from bitbucket

   $ git clone https://bitbucket.org/vrbacky/proceseq

2. Change directory to proceseq

   $ cd proceseq

3. Install proceseq
   
   $ python3 setup.py install --user


Contact
=======

If you have questions you can email
`Filip Vrbacky <mailto:vrbacky@fnhkcz>`__.

If you've discovered a bug or have a feature request, you can create an issue
on Bitbucket using the
`Issue Tracker <https://bitbucket.org/vrbacky/proceseq/issues>`__.

