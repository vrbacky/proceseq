Installation
============

The simplest way to install the latest stable release is via pip

   $ pip3 install proceseq --user
 
Linux
-----

1. Clone ProceSeq from bitbucket

   $ git clone https://bitbucket.org/vrbacky/proceseq

2. Change directory to proceseq

   $ cd proceseq

3. Install proceseq
   
   $ python3 setup.py install --user


