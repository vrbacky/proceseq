#!/usr/bin/env python3

"""Find combination of sequences with defined or maximum Hamming distance

It extracts combinations of specified number of sequences with maximum Hamming
distance from a fasta file (e.g. to find the best combination of NGS barcodes).
Only combinations of sequences with signal in both channels (e.g. to find
the best combination of Illumina MiSeq barcodes) can be extracted.

Example
-------

Find combinations of 4 sequencens with highest minimal Hamming distance from
MiSeq1-48.fasta file:

    $ most_distant.py -s 4 -1 MiSeq1-48.fasta

Find combinations of 4 sequencens with highest minimal Hamming distance
and signal in both channels from MiSeq1-48.fasta file:

    $ most_distant.py -s 4 -i -1 MiSeq1-48.fasta
"""

from argparse import ArgumentParser
from proceseq import most_distant
import sys

def get_arg_parser():
    parser = ArgumentParser(
        description="Find combination of "
                    "sequences with maximum Hamming distance.")
    parser.add_argument("-s", "--sequences", dest="count", default=6, type=int,
                        metavar='N',
                        help="number of sequences in output combinations")
    parser.add_argument("-1", dest="filename", type=str,
                        metavar='Str', default=None,
                        help="path to the input file containing sequences")
    parser.add_argument("-i", "--illumina", dest="illumina",
                        action="store_true", help="check for signal color "
                        "heterogeneity (for Illumina MiSeq barcodes)")
    parser.add_argument("-m", "--minheterogeneity", dest="heterogeneity",
                        default=0.15, type=float, metavar='N',
                        help="minimal signal color heterogeneity "
                        "(default = 0.15)")
    parser.add_argument("-q", "--quiet", dest="quiet", action="store_true",
                        help="don't send information messages to stdout")

    return parser


if __name__ == '__main__':
    parser = get_arg_parser()
    args = parser.parse_args()

    if args.filename is None:
        exit("No input file provided.")

    distance = most_distant.MostDistant(
        args.count, args.filename, args.illumina, args.quiet,
        args.heterogeneity)
    distance.most_distant()

    print("Finished!")
