#!/usr/bin/env python3

from Bio import SeqIO
from collections import Counter
from itertools import combinations
import operator
import sys


class MostDistant():
    """Extract combinations of sequences with highest minimal Hamming distance.

    Parameters
    ----------
    count : int
        Number of sequences in extracted lists (required)
    sequences : str or dict
        Path to a fasta file containing input sequences or
        dictionary containing sequences (name : sequence) (required)
    illumina_colors : boolean
        Check color heterogeneity for Illumina MiSeq analysis (default = False)
    quiet : boolean
        Don't send information messages to stdout (default = False)

    Example
    -------
    ::

        from mostdistant import MostDistant

        distance = MostDistant(4, "sequences.fa", illumina_colors=True,
                                  quiet=True)
        distance.most_distant()

    Attributes
    ----------
    distances : dict
        combinations of sequences (set of tuples) grouped according to their
        minimal Hamming distance (key)
    final_distance : int
        Minimum Hamming distance in output combinations
    final_sequences : list of lists
        Sorted list of sequence combinations with Hamming distance specified
        in final_sequences attribute
    """

    def __init__(self, count=None, sequences=None, illumina_colors=False,
                 quiet=False, heterogeneity=0.15):
        if type(sequences) is dict:
            self.sequences = sequences
        elif type(sequences) is str:
            self.prepare_seqs(sequences)
        else:
            raise TypeError("Sequences parameter must be a dictionary " +
                            "or a string!")
        self.count = count
        self.illumina_colors = illumina_colors
        self.quiet = quiet
        self.heterogeneity = heterogeneity

    def most_distant(self, send2stdout=True):
        """Find most distant sequences.

        Find most distant sequences and run color heterogeneity test
        for Illumina sequencers if required.

        Parameters
        ----------
            send2stdout : boolean
                Send final result to stdout (default = True)

        Returns
        -------
            list
                [Maximum distance, combinations of sequences as sorted lists]
        """

        if self.count is None or self.sequences is None:
            raise ValueError("'count' or 'sequence' parameter hasn't been" +
                             " defined. Distance cannot be calculated.")

        self.count_distances()

        if self.illumina_colors:
            most_distant_seqs, max_distance = self.color_heterogeneity()
        else:
            most_distant_seqs, max_distance = self.find_distant_sequences()

        self.final_sequences = most_distant_seqs
        self.final_distance = max_distance

        if send2stdout is True:
            self.print_results()

        return (max_distance, most_distant_seqs)

    def prepare_seqs(self, path2fasta=None):
        """Read input fasta file and convert it to dictionary name : sequence

        Parse input fasta file, convert it to dictionary with names as keys
        and sequences as values and save it to sequences attribute.

        Parameters
        ----------
            path2fasta : str
                Path to input fasta file

        Returns
        -------
            dict
                Dictionary of sequences (names as keys, sequences as values)
        """

        try:
            with open(path2fasta, "r") as file:
                seqs = {seq.id: str(seq.seq) for seq in SeqIO.parse(
                    file, "fasta")}
        except IOError:
            sys.exit("Error opening file {0}".format(path2fasta))
        except:
            sys.exit("Error parsing file {0}".format(path2fasta))

        self.sequences = seqs
        return seqs

    def count_distances(self, sequences=None):
        """Calculate distances of all possible combinations of sequences

        Return dictionary with Hamming distances as keys and sets containing
        combinations of sequences as values

        Parameters
        ----------
            sequences : dict or list
                Sequences to be analyzed (names as keys, sequences as values)
                if dictionary or list of nucleotide sequences

        Returns
        -------
            dict
                Dictionary of combinations of sequences grouped to set
                according to their minimal Hamming distance
        """

        if sequences is None:
            sequences = self.sequences

        elif type(sequences) is list:
            sequences = {seq: seq for seq in sequences}

        distances = {}

        seq_combinations = combinations(sequences.keys(), self.count)

        for combination in seq_combinations:

            mindist = self.count_mindist([sequences[x] for x in combination])
            if mindist in distances.keys():
                distances[mindist].add(combination)
            else:
                distances[mindist] = {combination}

        self.distances = distances
        return distances

    def hamdist(self, str1, str2):
        """Calculate Hamming distance between two strings(must be equal
        lengths)

        Parameters
        ----------
            str1 : str
                The first input string
            str2 : str
                The second input string

        Returns
        -------
            int
                Hamming distance of the two input strings
        """

        assert len(str1) == len(str2), ("Hamming distance is defined only "
                                        "for strings of the same length.")

        ne = operator.ne
        return sum(map(ne, str1, str2))

    def count_mindist(self, sequences):
        """Return lowest Hamming distance of all combinations of elements
        in a provided list

        Parameters
        ----------
            sequences : list
                List of sequences to be analyzed

        Returns
        -------
            int
                Lowest Hamming distance of all possible combinations
                of sequences in input list
        """
        return min({self.hamdist(*i) for i in combinations(sequences, 2)})

    def find_distant_sequences(self, sequences=None, distance=None):
        """Find combinations of sequences of specified or maximum distance

        Return combinations of sequences with Hamming distance provided as
        distance parameter. Most distant sequences are returned when no
        distance is specified. Output sequences are sorted. Distance is
        returned too.

        Parameters
        ----------
            sequences : dict
                dictionary with Hamming distances as keys and sets containing
                combinations of sequences as values. self.sequences
                (default = None)
            distance : int
                Distance to be extracted. Combinations with highest distance
                are extracted when None (default = None)

        Returns
        -------
            (list, int)
                sorted list of lists of sorted combinations of sequences
                and their minimal Hamming distance.
        """
        if sequences is None:
            sequences = self.distances

        if distance is None:
            try:
                distance = max(sequences.keys())
            except ValueError:
                exit("Cannot find any combination of sequences. "
                     "-s parameter is probably higher than number "
                     "of sequences in the input file.")

        try:
            return self._sort_names(sequences[distance]), distance
        except KeyError:
            return [], distance

    def color_heterogeneity(self, sequences=None):
        """Extract sequences with color heterogeneity (AT vs. CG) of at least
        15% only. Can be used to extract optimal combinations of MIDs.

        Parameters
        ----------
            sequences : dict
                dictionary with Hamming distances as keys and sets containing
                combinations of sequences as values. self.sequences
                (default = None)

        Returns
        -------
            (list, int)
                sorted list of lists of sorted combinations of sequences
                and their minimal Hamming distance.
        """
        if sequences is None:
            sequences = self.distances

        distances_list = reversed(sorted(sequences.keys()))

        max_distance = 0
        final_barcodes = []

        for distance in distances_list:
            for barcodes in self.distances[distance]:
                colors = self.compare_colors([self.sequences[x]
                                              for x in barcodes])
                if colors == 0:
                        final_barcodes.append(barcodes)
                elif colors == 1 and not self.quiet:
                    print("Combination {0} doesn't meet heterogeneity "
                          "criteria!".format(sorted(barcodes)))
                elif colors == 2 and not self.quiet:
                    print("Combination {0} contains illegal character(s)!".
                          format(sorted(barcodes)))

            if len(final_barcodes) == 0:
                continue
            else:
                max_distance = distance
                break

        return self._sort_names(final_barcodes), max_distance

    def compare_colors(self, sequences):
        """Check signal heterogeneity in both color channels for Illumina MiSeq

        Calculate CA vs. GT heterogeneity of all positions in provided
        list of sequences to make sure there is at least 15 % signal in
        both color channels at every position.

        Parameters
        ----------
            sequences : list
                List of sequences to be analyzed

        Returns
        -------
            int
                Returns 0 if color heterogeneity of all positions of sequnces
                provided as a list ih higher than 15 %. Returns 1 if it is
                lower. Returns 2 if any of the sequences contains an illegal
                character (non-ATCG).
        """

        number_of_sequences = len(sequences)

        for position in range(0, len(sequences[0])):
            counter = Counter()
            column = "".join([sequences[i][position]
                              for i in range(0, number_of_sequences)])
            counter.update(column.upper())

            if set(counter) - {"A", "C", "G", "T"}:
                return 2

            ac_number = counter["A"] + counter["C"]

            relative_ac = ac_number/number_of_sequences

            if (relative_ac < self.heterogeneity or
                    relative_ac > 1-self.heterogeneity):
                return 1

        return 0

    def print_results(self):
        """Print results to stdout"""

        if len(self.final_sequences) == 0 or self.final_distance == 0:
            print("No combination with minimal distance > 0.")
        else:
            print("Hamming distance: {0}".format(self.final_distance))
            [print(", ".join(i)) for i in self.final_sequences]

    def _sort_names(self, seq_combinations):
        """Sort sequences` names and their lists"""
        return sorted([sorted(i) for i in seq_combinations])
