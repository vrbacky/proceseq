.. proceseq documentation master file

.. include:: description.rst

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   get_started

.. toctree::
   :maxdepth: 2
   :caption: Usage

   usage
   api

.. toctree::
   :maxdepth: 2
   :caption: About

   about
